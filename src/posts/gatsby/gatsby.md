---
title: "The Great Gatsby Bootcamp"
date: "2019-07-28"
---

I just launched a new bootcamp!

![Mountain](./mountains.png)

## Topics Covered

1. Gatsby
2. GraphQL
3. React
