import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

const AboutPage = () => {
  return (
    <div>
      <Layout>
        <Head title="About" />
        <h1>About</h1>
        <p>Bachelor's degree in computer science and econometrics. </p>
        <p>8 months of commercial experience as front-end developer </p>
        <p>html5, css3, javascript, React, sass</p>
        <p>Want me in your team?</p>
        <p>
          <Link to="/contact">Contact me</Link>
        </p>
      </Layout>
    </div>
  )
}

export default AboutPage
