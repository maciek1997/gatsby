import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

const IndexPage = () => {
  return (
    <Layout>
      <Head title="Home" />
      <h1>Hello there.</h1>
      <h2>I'm Maciej Zając</h2>
      <p>Front-end developer with 6 months experience</p>
      <p>
        My technology stack: Javascript, React, Redux, Sass, html, css, webpack
      </p>
      <p>Other skills: BEM css methodology, webpack, continuos integration.</p>
    </Layout>
  )
}

export default IndexPage
