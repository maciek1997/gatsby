import React from "react"
import Layout from "../components/layout"
import Head from "../components/head"

const ContactPage = () => {
  return (
    <div>
      <Layout>
        <Head title="Contact" />
        <h1>Contact</h1>
        <p>
          My mail:{" "}
          <a href="mailto:maciej.zajac.197@gmail.com">
            maciej.zajac.197@gmail.com
          </a>
        </p>
      </Layout>
    </div>
  )
}

export default ContactPage
